INTRODUCTION

------------

This is our first project. 
The authors: Artin Ghalamkary, Karsten Bak Malle, Phillip Ravn Boe Jensen.

The purpose of this project is to develop and program our robot(Burgerbot). 
Main goal is to make the robot complete a maze as fast and clean as possible.




SIMULATION

----------

You run the simulation following this order:

    1. Clone the git repository.
    
    2. Open a terminal, go to catkin_ws using cd (cd catkin_ws).
    
    3. When you're in catkin_ws, use the command "catkin_make".
    
    4. In terminal write "roslaunch turtlebot3_gazebo turtlebot3_maze.launch"
    
    5. Now your Gazebo should open up with our customized maze.

    6. Open new terminal and write "roslaunch turtlebot3_gazebo turtlebot3_simulation.launch

    7. Enjoy!

# README template

Check the documentation for Markdown in GitLab to format your readme file.  
https://docs.gitlab.com/ee/user/markdown.html

## Table of contents

TOCs are optional but appreciated for lengthy README files.

[[_TOC_]]


## Introduction

The introduction (required) shall consist of a brief paragraph or two that summarizes the purpose and function of this project. This introduction may be the same as the first paragraph on the project page.  

The introduction should include a link to the project page and issue queue. If the project is a sandbox, these links should go to the sandbox until promotion.  


## Requirements

The requirements section (required) shall make it clear whether this project requires anything outside of Drupal core to work (modules, libraries, etc). List all requirements here, including those that follow indirectly from another module, etc. The idea is to inform the users about what is required, so that everything they need can be procured and included in advance of attempting to install the module. If there are no requirements, write "No special requirements".


## Recommended modules

This optional section lists modules that are not required, but that may enhance the usefulness or user experience of your project. Make sure to describe the benefits of enabling these modules.


## Installation

The installation section (required*) shall make it clear how to install the module. However, if the steps to install the module follow the standard instructions, don't reinvent the wheel — simply provide a link and explain in detail any steps that may diverge from these steps.

Consider replacing this section with a standalone INSTALL.txt file if your installation instructions are especially complex.


## Configuration

The configuration section (required) is necessary even when little configuration is required. Use this section to list special notes about the configuration of this module – including but not limited to permissions. 


## Troubleshooting & FAQ

These sections are optional. If present, they should address questions that are asked frequently in the issue queue (this will save you time in the future). Outline common problems that people encounter along with solutions (links are okay if the steps are long, but it is often helpful to provide a summary since links sometimes go stale).


## Maintainers

This section is optional and should replace any pre-existing standalone MAINTAINERS.txt file.


